package io.piveau.visualisationwrapper;

import io.vertx.config.ConfigRetriever;
import io.vertx.config.ConfigRetrieverOptions;
import io.vertx.config.ConfigStoreOptions;
import io.vertx.core.Future;
import io.vertx.core.*;
import io.vertx.core.http.HttpServer;
import io.vertx.core.json.JsonArray;
import io.vertx.core.json.JsonObject;
import io.vertx.core.logging.Logger;
import io.vertx.core.logging.LoggerFactory;
import io.vertx.ext.web.Router;
import io.vertx.ext.web.RoutingContext;
import io.vertx.ext.web.templ.freemarker.FreeMarkerTemplateEngine;

import java.util.Arrays;

public class MainVerticle extends AbstractVerticle {

    private static final Logger LOGGER = LoggerFactory.getLogger(MainVerticle.class);

    private FreeMarkerTemplateEngine engine;

    private JsonObject config;

    public static void main(String[] args) {
        String[] params = Arrays.copyOf(args, args.length + 1);
        params[params.length - 1] = MainVerticle.class.getName();
        Launcher.executeCommand("run", params);
    }


    @Override
    public void start() {
        LOGGER.info("Starting MainVerticle");
        Future<Void> steps = loadConfig()
                .compose(this::startServer);
    }


    private Future<Void> startServer(JsonObject config) {
        engine =  FreeMarkerTemplateEngine.create(vertx);
        Future<Void> future = Future.future();
        Integer port = config.getInteger("PORT");
        HttpServer server = vertx.createHttpServer();
        Router router = Router.router(vertx);
        router.get("/").handler(this::indexHandler);
        server.requestHandler(router).listen(port);
        LOGGER.info("Listening on port " + port.toString());
        return future;
    }

    private Future<JsonObject> loadConfig() {
        ConfigStoreOptions envStoreOptions = new ConfigStoreOptions()
                .setType("env")
                .setConfig(new JsonObject().put("keys", new JsonArray()
                        .add("PORT")
                        .add("BASE_URI")
                ));

        ConfigStoreOptions fileStoreOptions = new ConfigStoreOptions()
                .setType("file")
                .setConfig(new JsonObject().put("path", "conf/config.json"));

        ConfigRetriever retriever = ConfigRetriever
                .create(vertx, new ConfigRetrieverOptions()
                        .addStore(fileStoreOptions)
                        .addStore(envStoreOptions));


        Future<JsonObject> future = Future.future();
        retriever.getConfig(ar -> {
            if(ar.failed()) {
                future.failed();
            } else {
                config = ar.result();
                future.complete(ar.result());
            }
        });
        return future;
    }


    private void indexHandler(RoutingContext context) {
        String file = context.request().getParam("file");

        JsonObject data = new JsonObject().put("baseUri", config.getString("BASE_URI"));

        data.put("file", file);

        engine.render(data, "templates/index.ftl", res -> {
            if (res.succeeded()) {
                context.response().end(res.result());
            } else {
                context.fail(res.cause());
            }
        });
    }

}
