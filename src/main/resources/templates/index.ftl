<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta name="generator" content="piveau" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>European Data Portal Visualisation</title>
    <link rel="shortcut icon" href="${baseUri}/sites/all/themes/ec_resp_edp/favicon.ico" />

</head>

<body>

<#if file??>
    <h3>Visualisation is loading ...</h3>
    <p>
        <a href="#" id="preview-button" class="file-preview"
           data-file="${file}">If Visusalisation does not load within some seconds click here.</a>
    </p>

    <script id="vt_initializor" src="${baseUri}/vtool/javascripts/require.js" data-preprocessor-url="${baseUri}/visualization-tool" data-main="${baseUri}/vtool/javascripts/main" async> </script>
<#else >
    Please pass the file parameter.
</#if>

<style>
    body {
        margin: 20px;
        background-color: #fff;
        color: #333;
        font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
        font-size: 14px;
        line-height: 1.42857;
    }

    * {
        box-sizing: border-box;
    }

    .h1, .h2, .h3, .h4, .h5, .h6, h1, h2, h3, h4, h5, h6 {
        color: inherit;
        font-family: inherit;
        font-weight: 500;
        line-height: 1.1;
    }

    .h3, h3 {
        font-size: 24px;
    }

    .h1, .h2, .h3, h1, h2, h3 {
        margin-bottom: 10px;
        margin-top: 20px;
    }

    a {
        color: #337ab7;
        text-decoration: none;
    }
    a {
        background-color: transparent;
    }

    a:focus, a:hover {
        color: #23527c;
        text-decoration: underline;
    }

    a:active, a:hover {
        outline: 0 none;
    }

    p {
        margin: 0 0 10px;
    }
</style>
</html>