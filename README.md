# EDP2 Visualisation Wrapper

This services integrates the EDP1 visualisation service in the EDP2 architecture.

## Build:

Requirements: 
 * Git
 * Maven
 * Java

```bash
$ git clone https://gitlab.com/european-data-portal/hub/visualisation-wrapper.git
$ cd visualisation-wrapper
$ mvn package
```

## Run
```bash
$ cp conf/config.sample.json conf/config.json
$ java -jar target\visualisation-wrapper-fat.jar
```

## Docker
Build docker image:
```bash
$ docker build -t edp/visualisation-wrapper .
```

Run docker image:
```^bash
$ docker run -it -p 8090:8090 edp/visualisation-wrapper 
```

